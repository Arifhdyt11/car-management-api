# BINAR FSW5 Challenge-06 Kelompok 3  
 
## Anggota Kelompok  
1. Muhamad Arif Hidayat  
2. Robiata Tsania Salsabila  
3. Hafid Zaeny  
 
## Management Database
- `yarn db:create` digunakan untuk membuat database
- `yarn db:drop` digunakan untuk menghapus database
- `yarn db:migrate` digunakan untuk menjalankan database migration
- `yarn db:seed` digunakan untuk melakukan seeding
- `yarn db:rollback` digunakan untuk membatalkan migrasi terakhir



## Running Server 
```sh
yarn dev
```

## User Super Admin  
email    : superadmin@gmail.com 
password : superadmin 
 
## Open API Documentation 
http 
https://documenter.getpostman.com/view/20451659/UyxgJo8s