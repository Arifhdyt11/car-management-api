const express = require("express");
const res = require("express/lib/response");
const controllers = require("../app/controllers");
const apiRouter = express.Router();


// login as superadmin, admin, or member
apiRouter.post(
  "/api/v1/login",
  controllers.api.v1.userController.login
);

// whoami
apiRouter.get(
  "/api/v1/whoami",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.userController.whoAmI
);


//destroy users from db
apiRouter.delete(
  "/api/v1/logout/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isSuperAdmin,
  controllers.api.v1.userController.destroy
);

// add admin by superadmin only
apiRouter.put(
  "/api/v1/users/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isSuperAdmin,
  controllers.api.v1.userController.update
);

// register as member
apiRouter.post(
  "/api/v1/register",
  controllers.api.v1.userController.register
);


// read all car
apiRouter.get(
  "/api/v1/cars",
  controllers.api.v1.carController.list
);

// create car by admin or superadmin
apiRouter.post(
  "/api/v1/cars",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.create
);

// read one car 
apiRouter.get(
  "/api/v1/cars/:id",
  controllers.api.v1.carController.show
);

// update car by admin or superadmin
apiRouter.put(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.update
);

// delete car by admin or superadmin
apiRouter.delete(
  "/api/v1/cars/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isAdminOrSuperAdmin,
  controllers.api.v1.carController.makeCarDeleted
);

// destroy car by superadmin
apiRouter.delete(
  "/api/v1/carsdestroy/:id",
  controllers.api.v1.userController.authorize,
  controllers.api.v1.userController.isSuperAdmin,
  controllers.api.v1.carController.makeCarDestroy
);




apiRouter.get("/api/v1/errors", () => {
  throw new Error("The Industrial Revolution and its consequences have been a disaster for the human race.");
});

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;
